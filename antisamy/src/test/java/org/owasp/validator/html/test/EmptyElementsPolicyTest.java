package org.owasp.validator.html.test;

import org.custommonkey.xmlunit.XMLTestCase;
import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.Policy;


/**
 * Tests for the configuration in the policy file of which empty elements may be preserved.
 */
public class EmptyElementsPolicyTest extends XMLTestCase
{
    public void testDefaultElementsArePreserved() throws Exception {
        AntiSamy as = new AntiSamy(Policy.getInstance(getClass().getResourceAsStream("/test-empty-elements-policy.xml")));

        String domInput = "<root><br></br><hr></hr><a></a><img></img><link></link><iframe></iframe><script></script><object></object><applet></applet><frame></frame><base></base><param></param><meta></meta><input></input><textarea></textarea><embed></embed><basefont></basefont><col></col></root>";
        assertXMLEqual(domInput, as.scan(domInput).getCleanHTML());

        // The SAX input does not include many of the elements because it has peculiar handling of certain elements and fails to close the tags (or more likely isn't respecting the XHTML directive).
        String saxInput = "<root><a></a><iframe></iframe><script></script><object></object><applet></applet><textarea></textarea><embed></embed></root>";
        assertXMLEqual(saxInput, as.scan(saxInput, AntiSamy.SAX).getCleanHTML());
    }

    public void testOverridingDefaultElementSetting() throws Exception {
        AntiSamy as = new AntiSamy(Policy.getInstance(getClass().getResourceAsStream("/test-empty-elements-override-policy.xml")));
        String domInput = "<root><br></br><hr></hr><a></a><img></img><link></link><iframe></iframe><script></script><object></object><applet></applet><frame></frame><base></base><param></param><meta></meta><input></input><textarea></textarea><embed></embed><basefont></basefont><col></col></root>";
        String domExpected = domInput.replace("<iframe></iframe><script></script>", "");

        assertXMLEqual(domExpected, as.scan(domInput).getCleanHTML());

        // The SAX input does not include many of the elements because it has peculiar handling of certain elements and fails to close the tags (or more likely isn't respecting the XHTML directive).
        String saxInput = "<root><a></a><iframe></iframe><script></script><object></object><applet></applet><textarea></textarea><embed></embed></root>";
        String saxExpected = saxInput.replace("<iframe></iframe><script></script>", "");
        assertXMLEqual(saxExpected, as.scan(saxInput, AntiSamy.SAX).getCleanHTML());
    }

    public void testSpecifyingNewEmptyElements() throws Exception {
        AntiSamy as = new AntiSamy(Policy.getInstance(getClass().getResourceAsStream("/test-extra-empty-elements-policy.xml")));
        String input = "<root><p></p></root>";
        assertXMLEqual(input, as.scan(input).getCleanHTML());
        assertXMLEqual(input, as.scan(input, AntiSamy.SAX).getCleanHTML());
    }

    /**
     * Test to ensure that the SAX parser drops empty elements in the same manner as the DOM Parser for the default configuration.
     *
     * @throws Exception
     */
    public void testSaxParserStripsEmptyElements() throws Exception {
        AntiSamy as = new AntiSamy(Policy.getInstance(getClass().getResourceAsStream("/antisamy.xml")));
        String input = "<div><img src=\"apple.png\"/><span/></div><p/>";
        String expected = "<div><img src=\"apple.png\" /></div>";

        // The SAX expected result is different because it has peculiar handling of img elements and fails to close the tag (or more likely isn't respecting the XHTML directive).
        String expectedSax = "<div><img src=\"apple.png\"></div>";

        assertEquals(expected, as.scan(input).getCleanHTML());
        assertEquals(expectedSax, as.scan(input, AntiSamy.SAX).getCleanHTML());
    }
}
